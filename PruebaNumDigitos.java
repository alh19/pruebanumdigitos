package pruebanumdigitos;

import java.util.Scanner;

/**
 *
 * @author alber
 */
public class PruebaNumDigitos {
    
    public static void main(String[] args) {
        
        Scanner entrada = new Scanner(System.in);
        int num, resultado=1;
        
        System.out.print("Introduce número: ");
        num = entrada.nextInt();
        
        while(num>10){
            num=num/10;
            resultado++;
        }
        System.out.print("El número de dígitos es: "+resultado);
    }
}
